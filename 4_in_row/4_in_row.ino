#include <UTFT.h>

#define LEFTCLICK 6
#define RIGHTCLICK 7
#define BUT1 8
#define SAFE 9
#define RESET 10
extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];
UTFT GLCD(TFT32MEGA_2,38,39,40,41);

const int SQUARE = 40;

int lchoice = 1;
int choice = 1;
int player = 0;
int count = 0;
int tempplayer = 0;
int score1 = 0;
int score2 = 0;

String printstring = "";

int table[6][7] =
 {{0, 0, 0, 0, 0, 0, 0},
 {0, 0, 0, 0, 0, 0, 0},
 {0, 0, 0, 0, 0, 0, 0},
 {0, 0, 0, 0, 0, 0, 0},
 {0, 0, 0, 0, 0, 0, 0},
 {0, 0, 0, 0, 0, 0, 0}};

boolean LBR = LOW;
boolean CBR = LOW;
boolean LBL = LOW;
boolean CBL = LOW;
boolean LB1 = LOW;
boolean CB1 = LOW;
boolean LB3 = LOW;
boolean CB3 = LOW;

//Print global score
void getScore()
{ 
  GLCD.setFont(SevenSegNumFont);
  GLCD.setColor(VGA_RED); 
  GLCD.printNumI(score1, 9*SQUARE, 4*SQUARE+16);
  GLCD.setColor(VGA_YELLOW);
  GLCD.printNumI(score2, 10*SQUARE, 4*SQUARE+16);
  GLCD.setFont(SmallFont); 
}

int getHeight()
{
  if (table[0][choice-1] == 0)
  {
    for (int j = 0; j < 5; j++)
    {
      if (table[j+1][choice-1] != 0)
      {
        return j;
      }
    }
    return 5;
  }
  return -1;
}

void printMessage(String str)
{
  GLCD.setColor(VGA_YELLOW);
  GLCD.print(str, CENTER, 7*SQUARE + 4);
  delay(2000);
  GLCD.setColor(VGA_BLACK);
  GLCD.fillRect(0,7*SQUARE+1,12*SQUARE-1,8*SQUARE-1);
}

void changePlayer(int pl)
{
  if (player == 0)
  {
    GLCD.setColor(VGA_RED);
  }
  if (player == 1)
  {
    GLCD.setColor(VGA_YELLOW);
  }
  GLCD.setFont(SevenSegNumFont);
  GLCD.printNumI(player+1, 10*SQUARE, 2*SQUARE);
  GLCD.setFont(SmallFont);
}

void makeMove()
{
  int y = getHeight();
  if (y != -1)
  {
    Serial.print(choice-1);
    Serial.println(y);
    table[y][choice-1] = player + 1;
    for (int j = 0; j < 6; j++)
    {
      for (int i = 0; i < 7; i++)
      {
        Serial.print(table[j][i]);
      }
      Serial.println("");
    }
    if (player == 0)
    {
      GLCD.setColor(VGA_RED);
    }
    if (player == 1)
    {
      GLCD.setColor(VGA_YELLOW);
    }
    GLCD.fillCircle(choice*SQUARE + SQUARE/2, (y+1)*SQUARE + SQUARE/2, 16);
    checkWin();
    player = (player + 1)%2;
    changePlayer(player);
    Serial.println("Finished");
  }
  if (y == -1)
  {
    printMessage("You can't do it");
  }
}

void newArrow(int x, int lx)
{
  GLCD.setColor(VGA_BLACK);
  GLCD.fillRect(lx*SQUARE,15, (lx+1)*SQUARE, SQUARE-1);
  GLCD.setColor(VGA_WHITE);
  GLCD.drawLine(x*SQUARE + SQUARE/2, 15, x*SQUARE + SQUARE/2, SQUARE-5);
  GLCD.drawLine(x*SQUARE + SQUARE/2, SQUARE-5, x*SQUARE + SQUARE/2-5, SQUARE-10);
  GLCD.drawLine(x*SQUARE + SQUARE/2, SQUARE-5, x*SQUARE + SQUARE/2+5, SQUARE-10);
}

void createField()
{
  GLCD.setColor(VGA_BLUE);
  GLCD.fillRoundRect(1*SQUARE,1*SQUARE,8*SQUARE-1,7*SQUARE-1);
  GLCD.drawRoundRect(10*SQUARE-11,2*SQUARE-20,11*SQUARE+6,4*SQUARE-25);
  GLCD.drawRoundRect(9*SQUARE-11,4*SQUARE-4,11*SQUARE,6*SQUARE-12);
  GLCD.setColor(VGA_BLACK);
  for (int i = 1; i < 8; i++)
  {
    for (int j = 1; j < 7; j++)
    {
      GLCD.fillCircle(i*SQUARE + SQUARE/2, j*SQUARE + SQUARE/2, 16);
    }
  }
  GLCD.setColor(VGA_YELLOW);
  GLCD.setFont(BigFont);
  GLCD.print(String("SCORE"), 9*SQUARE-5, 4*SQUARE);
  GLCD.setFont(SmallFont);
  GLCD.print(String("Player"), 10*SQUARE-5, 2*SQUARE-16);
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 7; j++)
    {
      table[i][j] = 0;
    }
  }
 player = 0;
}

//
void victory(int p)
{
  printstring = "Winner is " + String(p) + " player";
  GLCD.setFont(BigFont);
  GLCD.setColor(VGA_BLACK);
  GLCD.fillRect(0,0,479,319);
  GLCD.setColor(VGA_YELLOW);
  GLCD.print(printstring, CENTER, 4*SQUARE-8);
  GLCD.setFont(SmallFont);
  delay(10000);
  GLCD.setColor(VGA_BLACK);
  GLCD.fillRect(0,0,479,319);
}

//
void winSequence(int x1, int y1, int x2, int y2, int pl)
{
  GLCD.setColor(VGA_BLACK);
  GLCD.drawLine(x1, y1, x2, y2);
  GLCD.drawLine(x1+1, y1, x2+1, y2);
  GLCD.drawLine(x1, y1+1, x2, y2+1);
  GLCD.drawLine(x1, y1-1, x2, y2-1);
  GLCD.drawLine(x1-1, y1, x2-1, y2);
  printstring = "Winner is " + String(pl+1) + " player";
  printMessage(printstring);
  if (pl == 0)
  {
    score1 += 1;
  }
  if (pl == 1)
  {
    score2 += 1;
  }
  reset();
  getScore();
  if (score1 == 3)
  {
    victory(1);
  }
  if (score2 == 3)
  {
    victory(2);
  }
  player = -1;
}

//Check table for win sequence
void checkWin()
{
  //4-in-row line
  for (int i = 0; i < 6; i++)
  {
    count = 0;
    for (int j = 0; j < 7; j++)
    {
      int temp = table[i][j];
      if (temp == 0)
      {
        count = 0;
        tempplayer = 0;
      }
      else
      {
        if (tempplayer == temp)
        {
          count = count + 1;
        }
        else
        {
          tempplayer = temp;
          count = 1;
        }
      }
      if (count == 4)
      {
        winSequence((j-2)*SQUARE+SQUARE/2, (i+1)*SQUARE+SQUARE/2, (j+1)*SQUARE+SQUARE/2, (i+1)*SQUARE+SQUARE/2, tempplayer-1);
        
      }
    }
  }
  //4-in-row column
  for (int j = 0; j < 7; j++)
  {
    count = 0;
    for (int i = 0; i < 6; i++)
    {
      int temp = table[i][j];
      if (temp == 0)
      {
        count = 0;
        tempplayer = 0;
      }
      else
      {
        if (tempplayer == temp)
        {
          count = count + 1;
        }
        else
        {
          tempplayer = temp;
          count = 1;
        }
      }
      if (count == 4)
      {
        winSequence((j+1)*SQUARE+SQUARE/2, (i-2)*SQUARE+SQUARE/2, (j+1)*SQUARE+SQUARE/2, (i+1)*SQUARE+SQUARE/2, tempplayer-1);
      }
    }
  }
  //4-in-row diagonal top left
  for (int q = 0; q < 6; q++)
  {
    count = 0;
    int i = 0;
    int j = 0;
    if (q < 4)
    {
      j = q;
    }
    else
    {
      i = q - 3;
    }
    while(i < 6)
    {
      if (j == 7)
      {
        break;
      }
      int temp = table[i][j];
      if (temp == 0)
      {
        count = 0;
        tempplayer = 0;
      }
      else
      {
        if (tempplayer == temp)
        {
          count = count + 1;
        }
        else
        {
          tempplayer = temp;
          count = 1;
        }
      }
      if (count == 4)
      {
        winSequence((j-2)*SQUARE+SQUARE/2, (i-2)*SQUARE+SQUARE/2, (j+1)*SQUARE+SQUARE/2, (i+1)*SQUARE+SQUARE/2, tempplayer-1);
      }
      i = i + 1;
      j = j + 1; 
    }
  }
  //4-in-row diagonal top right
  for (int q = 0; q < 6; q++)
  {
    count = 0;
    int i = 0;
    int j = 6;
    if (q < 4)
    {
      j = j - q;
    }
    else
    {
      i = q - 3;
    }
    while(i < 6)
    {
      if (j == -1)
      {
        break;
      }
      int temp = table[i][j];
      if (temp == 0)
      {
        count = 0;
        tempplayer = 0;
      }
      else
      {
        if (tempplayer == temp)
        {
          count = count + 1;
        }
        else
        {
          tempplayer = temp;
          count = 1;
        }
      }
      if (count == 4)
      {
        winSequence((j+1)*SQUARE+SQUARE/2, (i+1)*SQUARE+SQUARE/2, (j+4)*SQUARE+SQUARE/2, (i-2)*SQUARE+SQUARE/2, tempplayer-1);
      }
      i = i + 1;
      j = j - 1; 
    }
  }
}

//Game reset (global score doesn't change)
void reset()
{
  createField();
  player = 0;
  choice = 1;
  changePlayer(player);
  newArrow(choice, lchoice);
  lchoice = choice;
}

//For pressing buttons
boolean debounce(boolean last, int BTN)
{
  boolean current = digitalRead(BTN);
  if (last != current)
  {
    delay(5);
    current = digitalRead(BTN);
    return current;
  }
  return current;
}

//Run once in the beginning
void setup() {
  GLCD.InitLCD();
  GLCD.setFont(SmallFont);
  GLCD.clrScr();
  pinMode (LEFTCLICK, INPUT_PULLUP);
  pinMode (RIGHTCLICK, INPUT_PULLUP);
  pinMode (BUT1, INPUT_PULLUP);
  pinMode (SAFE,INPUT_PULLUP);
  pinMode (RESET,INPUT_PULLUP);
  createField();
  changePlayer(player);
  newArrow(choice, lchoice);
  getScore();
  Serial.begin(9600);
}

//Run repeatedly
void loop() 
{
  if (SAFE)
  {
    CBR = debounce(LBR, RIGHTCLICK);
    CBL = debounce(LBL, LEFTCLICK);
    CB1 = debounce(LB1, BUT1);
    CB3 = debounce(LB3, RESET);
    // ->
    if ((LBR == LOW) && (CBR == HIGH))
    {
      choice = (++choice)%7;
      if (choice == 0)
      {
        choice = 7;
      }
      newArrow(choice, lchoice);
      lchoice = choice;
    }
    // <-
    if ((LBL == LOW) && (CBL == HIGH))
    {
      choice = --choice;
      if (choice == 0)
      {
        choice = 7;
      }
      newArrow(choice, lchoice);
      lchoice = choice;
    }
    // Button
    if ((LB1 == LOW) && (CB1 == HIGH))
    {
      makeMove();
    }
    if ((LB3 == LOW) && (CB3 == HIGH))
    {
      reset();
    }
    LBR = CBR;
    LBL = CBL;
    LB1 = CB1;
    LB3 = CB3;
  }
}
