#include <pcmConfig.h>
#include <pcmRF.h>
#include <TMRpcm.h>
#include <SD.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include "pitches.h"
#define SD_PIN 4  //using digital pin 4 on arduino nano 328, can use other pins
#define SPEAK_PIN 9

const int RED = 2;
const int BLACK = 7;
const int WHITE = 8;
// Защита от возможных проблем программы
const int SAFE = 14;
char** song = new char* [10];
int i = 0;
int n;
int mode = 0;

int melody[] = {
  B2, A2, G2, D2,
  B2, A2, G2, D2,
  B2, A2, G2, D2,
  C3, B2, A2, B2,
  B2, A2, G2, D2,
  B2, A2, G2, D2,
  B2, A2, G2, D2,
  C3, B2, A2, B2,
  G2
};

int noteDurations[] = {
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  2, 2, 2, 2,
  1
};

boolean LBR = LOW;
boolean CBR = LOW;
boolean LBB = LOW;
boolean CBB = LOW;
boolean LBW = LOW;
boolean CBW = LOW;
File root;
File entry;
TMRpcm tmrpcm;   // create an object for use in this sketch
  
LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей

File file;

// Функция воспроизведения мелодии (заданный вариант)
void playsong()
{
  for (int thisNote = 0; thisNote < 33; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(9);
  }
}

// Функция устранения дребезга
boolean debounce(boolean last, int BTN)
{
  boolean current = digitalRead(BTN);
  if (last != current)
  {
    delay(5);
    current = digitalRead(BTN);
    return current;
  }
  return current;
}

// Настройка, выполняется перед началом loop()
void setup()
{
  pinMode(RED, INPUT);
  pinMode(BLACK, INPUT);
  pinMode(WHITE, INPUT);
  pinMode(SAFE,INPUT);
  Serial.begin(9600);
  lcd.init();                     
  lcd.backlight();
  if (SAFE)
  {
    // Настройка SD-card
    lcd.print("SD card...");
    while (!SD.begin(SD_PIN)) {
      lcd.clear();
      lcd.print(" failed!");
      delay(100);
    }
    root = SD.open("/");
    entry =  root.openNextFile();
    entry.close();
    for (i=0; i<10; i++)
    {
      entry =  root.openNextFile();
      if(!entry)
      {
        break;
      }
      char* sname = entry.name();
      song[i] = new char[strlen(sname)+1];
      strcpy(song[i], sname);
      Serial.println(i);
      Serial.println(song[i]);
      entry.close();
    }
    n = i;
    i = 0;
    Serial.println(song[1]);
    lcd.setCursor(0,1);
    lcd.print("Connected");
    delay(1000);
    lcd.clear();
    lcd.print("WELCOME");
    tmrpcm.speakerPin = SPEAK_PIN; 
    SD.begin(SD_PIN);
    tmrpcm.volume(1);
    tmrpcm.play("startup.wav"); //the sound file "music" will play each time the arduino powers up, or is reset
  }
}

void loop()
{
  if (SAFE)
  {
    CBR = debounce(LBR, RED);
    CBB = debounce(LBB, BLACK);
    CBW = debounce(LBW, WHITE);
    // Красная кнопка
    if ((LBR == LOW) && (CBR == HIGH))
    {
      mode = 1;
      lcd.clear();
      lcd.print("SPECIAL SONG");
    }
    // Черная кнопка
    if ((LBB == LOW) && (CBB == HIGH))
    {
      mode = 0;
      lcd.clear();
      i = (++i)%n;
      lcd.print(song[i]);
      Serial.println(i);
      Serial.println(song[i]);
    }
    // Белая кнопка
    if ((LBW == LOW) && (CBW == HIGH))
    {
      if(mode == 0)
      {
         tmrpcm.play(song[i]);    
         mode = 2;
      }
      if(mode == 1)
      {
        playsong();
      }
      if(mode == 2)
      {
        tmrpcm.pause();
      }
      
    }
    LBR = CBR;
    LBB = CBB;
    LBW = CBW;
  }
}

