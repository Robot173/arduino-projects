#include <DHT.h>
#include <DHT_U.h>
#include <SD.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

const int SD_PIN = 4;
#define RED 7 			// Пин красной кнопки
#define WHITE 6 		// Пин белой кнопки
#define SNS 2 			// Пин датчика сенсора 
#define SAFE 10  		// Защита от возможных проблем программы

int mode = 1;
unsigned long previousMillis = 0; 
int minute = 0; 
int hour = 0;


File res;

String currentmode;
String dataString;

int pmode = 1;			// Отвечает за номер комнаты
int h = 0;				// Влажность
int t = 0;				// Температура

byte degree[8] =
{
  B00111,
  B00101,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000,
};   					// Символ градуса

boolean LBR = LOW;
boolean CBR = LOW;
boolean LBW = LOW;
boolean CBW = LOW;

File root;
File file;
LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей
DHT dht(SNS, DHT11);

// Смена названия комнаты
void changemode(int mode)
{

  if(mode == 0)
  {
    currentmode = "Room   ";
  }
  if(mode == 1)
  {
    currentmode = "Bath   ";
  }
  if(mode == 2)
  {
    currentmode = "Kitchen";
  }
  if(mode == 3)
  {
    currentmode = "Hallway";
  }
  lcd.setCursor(0,0);
  lcd.print(currentmode);
}

// Функция устранения дребезга
boolean debounce(boolean last, int BTN)
{
  boolean current = digitalRead(BTN);
  if (last != current)
  {
    delay(5);
    current = digitalRead(BTN);
    return current;
  }
  return current;
}

// Настройка, выполняется перед началом loop()
void setup()
{
  Serial.begin(9600);
  pinMode(RED, INPUT);
  pinMode(SNS, INPUT);
  pinMode(WHITE, INPUT);
  pinMode(SAFE, INPUT);
  lcd.init();                     
  lcd.backlight();
  lcd.createChar(1, degree);
  if (SAFE)
  {
    // Настройка SD-card
    lcd.print("SD card...");
    while (!SD.begin(SD_PIN)) {
      lcd.clear();
      lcd.print(" failed!");
      delay(100);
    }
    root = SD.open("/");
    SD.begin(SD_PIN);
    lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("t:");
    lcd.setCursor(5,1);
    lcd.print("\1C");
    lcd.setCursor(8,1);
    lcd.print("H:");
    lcd.setCursor(13,1);
    lcd.print("%");
    changemode(mode);
  }
  
}

void loop()
{
  if (SAFE)
  {
    unsigned long currentMillis = millis();
    CBR = debounce(LBR, RED);
    CBW = debounce(LBW, WHITE);
    // Красная кнопка
    if ((LBR == LOW) && (CBR == HIGH))
    {
      mode = (++mode)%4;
      changemode(mode);
    }
    // Белая кнопка
    if ((LBW == LOW) && (CBW == HIGH))
    {
      if(mode == 0)
        {
          dataString = "------Room------";
        }
      if(mode == 1)
        {
          dataString = "------Bath------";
        }
      if(mode == 2)
        {
          dataString = "-----Kitchen-----";
        }
      if(mode == 3)
        {
          dataString = "-----Hallway-----";
        }
      res = SD.open("RESULTS.txt", FILE_WRITE);
      if (res)
      {
        res.println("\n");
        res.println(dataString);
        res.println("\n");
        lcd.setCursor(10,0);
        lcd.print("Ok");
        delay(1000);
        lcd.setCursor(10,0);
        lcd.print("  ");
        res.close();
      }
      else
      {
        lcd.setCursor(10,0);
        lcd.print("Error");
        delay(1000);
        lcd.setCursor(10,0);
        lcd.print("     ");
      }
    }
    LBR = CBR;
    LBW = CBW;
    h = dht.readHumidity();
    t = dht.readTemperature();
    lcd.setCursor(3,1);
    lcd.print(t);
    lcd.setCursor(11,1);
    lcd.print(h);
    if (currentMillis - previousMillis >= 60000) // Если прошла минута
    {
      minute = ++minute;
      previousMillis = currentMillis;
      res = SD.open("RESULTS.txt", FILE_WRITE);
      dataString = "";
      dataString += String(t);
      dataString += ("\t");
      dataString += String(h);

      Serial.println(dataString);
      if (res)
      {
        res.println(dataString);
        if (minute == 60)						// Если прошел час
          {
            hour = ++hour;
            res.println("");
            if (hour == 24)						// Если прошел день
              {
                res.println("\n");
                hour = 0;
              }
            minute = 0;
          }
        res.close();
      }
      else
      {
        Serial.println("Error");
      }
    }
  }
}

